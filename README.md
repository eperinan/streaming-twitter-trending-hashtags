# Streaming twitter trending hashtags

Application that performs searches for tweets that contains a given list of words (for example: "justin bieber", "star wars" and "real madrid") in real time. 

The goal of these searches are calculate the custom trending topic and print them in the screen every 10 seconds.

## Solution

To achieve this problem, I chose the following technologies:

* **Spark** as engine for large-scale data processing.
* **Spark Streaming Twitter** incorporates a connector based on **Twitter4j** to process tweets via streaming in real time.
* **Akka** with the actor pattern in order to separate the processing part to the visualization part.

## How to run the application

To run locally our application, we can choose between two options. However, for both options, we will have to pass the follow environment variables.

* TWITTER4J_CONSUMER_KEY : Consumer key obtained in the Twitter website.
* TWITTER4J_CONSUMER_SECRET : Consumer secret obtained in the Twitter website.
* TWITTER4J_ACCESS_TOKEN : Access token obtained in the Twitter website.
* TWITTER4J_ACCESS_SECRET: Access secret obtained in the Twitter website.
* TWITTER4J_SEARCH_PARAMS (Optional) : List of words to search separate with comma. **By default:** "justin bieber, star wars, real madrid"
* DATE_FORMAT (Optional) : Date format to print the date of the analysis. **By default:** "yyyy-mm-dd HH.mm.ss"
* WINDOW_TIME (Optional) : Time in seconds interval at which streaming data will be divided into batches. **By default:** 10
* MASTER (Optional): The master URL to connect to, such as "local" to run locally with one thread, "local[4]" to run locally with 4 cores, or "spark://master:7077" to run on a Spark standalone cluster. **By default:** "local[2]"

The first option available is running the application with `sbt`.

```
export TWITTER4J_CONSUMER_KEY=; export TWITTER4J_CONSUMER_SECRET=; export TWITTER4J_ACCESS_TOKEN=; export TWITTER4J_ACCESS_SECRET=; export TWITTER4J_SEARCH_PARAMS="justin bieber, star wars, real madrid"; sbt run```
```

The second option available is execute the *fat jar file*. For this execution, we will have to generate this file with the follow command previously.

```
sbt clean assembly
export TWITTER4J_CONSUMER_KEY=; export TWITTER4J_CONSUMER_SECRET=; export TWITTER4J_ACCESS_TOKEN=; export TWITTER4J_ACCESS_SECRET=; export TWITTER4J_SEARCH_PARAMS="justin bieber, star wars, real madrid"; java -jar target/solution.jar
```

#Output

This is the output that we will see if the application works fine and we pass the follow params ("marquez, argentinagp, BahrainGP, MotoGP").

Note: These results could be different when you run the application. We have to keep in mind, that we are analysing tweets in real time.

```
Tweets from 2018-11-08 21.11.10 to 2018-11-08 21.11.19

Position | Hashtag          | Count | Upward/Downward
---------|------------------|-------|----------------
1        | BahrainGP        | 5     | New
2        | F1               | 4     | New
3        | ArgentinaGP      | 2     | New
4        | MotoGP           | 1     | New
5        | Raikkonen        | 1     | New
6        | MotoGPxESPN      | 1     | New
7        | TermasDeRioHondo | 1     | New
8        | Gasado           | 1     | New
9        | Video            | 1     | New
10       | TermasClash      | 1     | New

Tweets from 2018-11-08 21.11.20 to 2018-11-08 21.11.29

Position | Hashtag           | Count | Upward/Downward
---------|-------------------|-------|----------------
1        | ArgentinaGP       | 6     | ↑ 2
2        | BahrainGP         | 4     | ↓ 1
3        | MotoGP            | 3     | ↑ 1
4        | Rossi             | 3     | New
5        | ARGmovistarMotoGP | 2     | New
6        | Marquez           | 2     | New
7        | F1                | 2     | ↓ 5
8        | RSspirit          | 1     | New
9        | motori            | 1     | New
10       | disgrace          | 1     | New

Tweets from 2018-11-08 21.11.30 to 2018-11-08 21.11.39

Position | Hashtag     | Count | Upward/Downward
---------|-------------|-------|----------------
1        | ArgentinaGP | 7     | =
2        | BahrainGP   | 6     | =
3        | MotoGP      | 4     | =
4        | F1          | 3     | ↑ 3
5        | Mueller     | 1     | New
6        | Washington  | 1     | New
7        | MM93        | 1     | New
8        | Motogp      | 1     | New
9        | Trump       | 1     | New
10       | Rossi       | 1     | ↓ 6

Tweets from 2018-11-08 21.11.40 to 2018-11-08 21.11.49

Position | Hashtag           | Count | Upward/Downward
---------|-------------------|-------|----------------
1        | BahrainGP         | 14    | ↑ 1
2        | ArgentinaGP       | 8     | ↓ 1
3        | F1                | 7     | ↑ 1
4        | MotoGP            | 6     | ↓ 1
5        | Rossi             | 2     | ↑ 5
6        | MotoGPxESPN       | 2     | New
7        | ARGmovistarMotoGP | 2     | New
8        | Gasado            | 2     | New
9        | ForzaFerrari      | 2     | New
10       | DrivenByEachOther | 1     | New
```
