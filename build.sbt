name := "streaming-twitter-trending-hashtags"

scalaVersion := "2.11.8"

lazy val V = new {
  val spark = "2.2.0"
  val akka = "2.5.11"
  val cliTools = "0.0.1"
}

libraryDependencies += "org.apache.spark" %% "spark-core" % V.spark
libraryDependencies += "org.apache.spark" %% "spark-streaming" % V.spark
libraryDependencies += "org.apache.spark" %% "spark-streaming-kinesis-asl" % V.spark
libraryDependencies += "org.apache.bahir" %% "spark-streaming-twitter" % V.spark
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % V.akka
libraryDependencies += "com.github.wookietreiber" %% "scala-cli-tools" % V.cliTools

def excludeSpecs(module: ModuleID): ModuleID =
  module.excludeAll(ExclusionRule(organization = "com.amazonaws"))

libraryDependencies ~= (_.map(excludeSpecs))

assemblyOutputPath in assembly := file("target/solution.jar")

assemblyMergeStrategy in assembly := {
  case PathList("org", "aopalliance", xs @ _*)      => MergeStrategy.last
  case PathList("javax", "inject", xs @ _*)         => MergeStrategy.last
  case PathList("javax", "servlet", xs @ _*)        => MergeStrategy.last
  case PathList("javax", "activation", xs @ _*)     => MergeStrategy.last
  case PathList("org", "apache", xs @ _*)           => MergeStrategy.last
  case PathList("com", "google", xs @ _*)           => MergeStrategy.last
  case PathList("com", "esotericsoftware", xs @ _*) => MergeStrategy.last
  case PathList("com", "codahale", xs @ _*)         => MergeStrategy.last
  case PathList("com", "yammer", xs @ _*)           => MergeStrategy.last
  case "about.html"                                 => MergeStrategy.rename
  case "META-INF/ECLIPSEF.RSA"                      => MergeStrategy.last
  case "META-INF/mailcap"                           => MergeStrategy.last
  case "META-INF/mimetypes.default"                 => MergeStrategy.last
  case "plugin.properties"                          => MergeStrategy.last
  case "log4j.properties"                           => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
