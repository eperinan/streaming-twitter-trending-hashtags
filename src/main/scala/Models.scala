package com.eperinan.twitter.streaming.models

import org.apache.spark.streaming.{Duration, Time}
import twitter4j.HashtagEntity

case class CountHashtag(hashtag: HashtagEntity, count: Int)
case class HashTag(value: String)
case class TopTenHashTags(hashTag: List[CountHashtag], time: Time)

case class CredentialsTwitter(
    consumerKey: String,
    consumerSecret: String,
    accessToken: String,
    accessTokenSecret: String)

case class ConfigApp(
    dateFormat: String,
    windowTime: Duration,
    master: String,
    searchParams: List[String],
    credentialsTwitter: CredentialsTwitter)
