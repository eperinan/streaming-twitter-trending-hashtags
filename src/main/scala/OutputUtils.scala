package com.eperinan.twitter.streaming

import java.time.{Instant, ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

import com.eperinan.twitter.streaming.models.{ConfigApp, CountHashtag, HashTag}
import org.apache.spark.streaming.Time
import scalax.cli.Table

object OutputUtils {

  def position(hashtag: HashTag, currentPosition: Int, prevHashTags: List[HashTag]): String = {
    prevHashTags.indexOf(hashtag) match {
      case -1                                            => "New"
      case oldPosition if oldPosition == currentPosition => "="
      case oldPosition if oldPosition > currentPosition =>
        s"↑ ${oldPosition - currentPosition}"
      case oldPosition if oldPosition < currentPosition =>
        s"↓ ${currentPosition - oldPosition}"
    }
  }

  def formatPosition(pos: Int): String = (pos + 1).toString

  def formatDate(time: Time, minus: Long)(implicit config: ConfigApp): String = {
    val formatter = DateTimeFormatter.ofPattern(config.dateFormat)
    val instant   = Instant.ofEpochMilli(time.milliseconds).minusSeconds(minus)
    ZonedDateTime.ofInstant(instant, ZoneId.of("UTC")).format(formatter)
  }

  def printTable(hashTag: List[CountHashtag], time: Time, prevHashTags: List[HashTag])(
      implicit config: ConfigApp): Unit = {

    Console.println(s"\nTweets from ${formatDate(time, 10)} to ${formatDate(time, 1)}\n")

    val table = Table("Position", "Hashtag", "Count", "Upward/Downward")

    hashTag.zipWithIndex foreach {
      case (CountHashtag(hashtag, count), pos) =>
        table.rows += Seq[String](
          formatPosition(pos),
          hashtag.getText,
          count.toString,
          position(HashTag(hashtag.getText), pos, prevHashTags))
    }

    table.print()

  }
}
