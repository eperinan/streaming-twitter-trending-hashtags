package com.eperinan.twitter.streaming

import akka.actor.{Actor, Props}
import com.eperinan.twitter.streaming.models._

object TopTenHashtagActor {
  def props(configApp: ConfigApp): Props = Props(new TopTenHashtagActor(configApp))
}

class TopTenHashtagActor(configApp: ConfigApp) extends Actor {

  private var state: List[HashTag] = Nil

  def receive = {
    case TopTenHashTags(hashTags, time) => {
      OutputUtils.printTable(hashTags, time, state)(configApp)
      state = hashTags.map(_.hashtag.getText).map(HashTag)
    }
    case _ => Console.println("Undefined case!")
  }
}
