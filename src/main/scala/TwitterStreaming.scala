package com.eperinan.twitter.streaming

import akka.actor.{ActorSystem, Props}
import com.eperinan.twitter.streaming.models._
import com.eperinan.twitter.streaming.Utils._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.StreamingContext
import twitter4j.{HashtagEntity, Status}
import twitter4j.auth.Authorization

object TwitterStreaming extends App {

  implicit val configApp: ConfigApp = buildConfig()

  val twitterAuth: Option[Authorization] = twitterAuthorization()
  val system                             = ActorSystem("TwitterStreamingActor")
  val topTenActor                        = system.actorOf(TopTenHashtagActor.props(configApp), name = "TopTenHashtagActor")

  val sparkConf = new SparkConf()
    .setAppName("TwitterStreaming")
    .setMaster(configApp.master)

  val ssc = new StreamingContext(sparkConf, configApp.windowTime)

  val twits: ReceiverInputDStream[Status] =
    TwitterUtils.createStream(ssc, twitterAuth, configApp.searchParams)

  val hashTags: DStream[HashtagEntity] = twits.flatMap(_.getHashtagEntities)

  val top10: DStream[CountHashtag] =
    hashTags
      .map(hashTag => (hashTag, 1))
      .reduceByKeyAndWindow(_ + _, configApp.windowTime)
      .transform(_.sortBy(_._2, ascending = false))
      .map {
        case (hashtag: HashtagEntity, count: Int) =>
          CountHashtag(hashtag, count)
      }

  top10.foreachRDD((rdd, time) => topTenActor ! TopTenHashTags(rdd.take(10).toList, time))

  ssc.start()
  ssc.awaitTermination()
}
