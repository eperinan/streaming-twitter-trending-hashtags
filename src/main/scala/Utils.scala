package com.eperinan.twitter.streaming

import com.eperinan.twitter.streaming.models._
import com.typesafe.config.ConfigFactory
import org.apache.spark.streaming.Seconds
import twitter4j.auth.{Authorization, OAuthAuthorization}
import twitter4j.conf.ConfigurationBuilder

object Utils {

  def buildConfig(): ConfigApp = {

    val cfg = ConfigFactory.load()

    val credentialsTwitter = CredentialsTwitter(
      cfg.getString("twitter4j.credentials.consumer-key"),
      cfg.getString("twitter4j.credentials.consumer-secret"),
      cfg.getString("twitter4j.credentials.access-token"),
      cfg.getString("twitter4j.credentials.access-tokenSecret")
    )

    ConfigApp(
      cfg.getString("utils.date-format"),
      Seconds(cfg.getInt("spark.window-time")),
      cfg.getString("spark.master"),
      cfg.getString("twitter4j.search-params").split(",").toList,
      credentialsTwitter
    )

  }

  def twitterAuthorization()(implicit config: ConfigApp): Option[Authorization] = {

    val cb = new ConfigurationBuilder()

    cb.setDebugEnabled(true)
      .setOAuthConsumerKey(config.credentialsTwitter.consumerKey)
      .setOAuthConsumerSecret(config.credentialsTwitter.consumerSecret)
      .setOAuthAccessToken(config.credentialsTwitter.accessToken)
      .setOAuthAccessTokenSecret(config.credentialsTwitter.accessTokenSecret)

    Some(new OAuthAuthorization(cb.build))

  }

}
